package com.yomahub.tlog.example;

import com.yomahub.tlog.task.jdk.TLogTimerTask;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.Timer;
import java.util.TimerTask;

@Component
public class TaskRunner implements CommandLineRunner {

    private Logger log = LoggerFactory.getLogger(this.getClass());

    @Override
    public void run(String... args) throws Exception {
        Timer timer = new Timer();
        timer.scheduleAtFixedRate(new TLogTimerTask() {
            @Override
            public void runTask() {
                log.info("hello,world");
            }
        }, 100, 1000);
    }
}
