package com.yomahub.tlog.example;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class TaskRunner {

    private Logger log = LoggerFactory.getLogger(this.getClass());

    @Scheduled(cron="0/1 * * * * ?")
    public void run() throws Exception {
        log.info("hello,world");
    }
}
