package com.yomahub.tlog.example.id;

import cn.hutool.core.lang.UUID;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.yomahub.tlog.id.TLogIdGenerator;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.http.HttpServletRequest;

public class TestIdGenerator extends TLogIdGenerator {

    @Autowired
    private HttpServletRequest request;

    @Override
    public String generateTraceId() {
        if (ObjectUtil.isNotNull(request) && ObjectUtil.isNotNull(request.getParameter("prefix"))){
            String id = StrUtil.format("{}_{}",request.getParameter("prefix"), UUID.fastUUID().toString());
            return id;
        }else{
            return UUID.fastUUID().toString();
        }
    }
}
